import paddle
from paddle.nn import Linear
import paddle.nn.functional as F
import numpy as np
import os
import random

# test paddle 
paddle.set_device('cpu')
x = paddle.to_tensor(np.random.uniform(-1, 1, [3, 2]).astype('float32'))
linear = Linear(2, 3)
y = linear(x)
print(y)
