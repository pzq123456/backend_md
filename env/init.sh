# Desc: Install python3.8 and pip3.8 
# 环境配置脚本 CentOS 8
VERSION=3.8.5
# wget https://www.python.org/ftp/python/${VERSION}/Python-${VERSION}.tgz
wget https://mirrors.huaweicloud.com/python/${VERSION}/Python-${VERSION}.tgz
tar -xf Python-${VERSION}.tgz
cd Python-${VERSION}
./configure --enable-optimizations

yum install libffi-devel -y # 修复ctypes 问题

make -j 2
sudo make altinstall

# test python3.8
python3.8 --version
# test pip3.8
pip3.8 --version






