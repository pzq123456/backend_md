# 基础环境配置记录

## 1. 
```bash
sudo dnf groupinstall 'development tools'
sudo dnf install bzip2-devel expat-devel gdbm-devel ncurses-devel openssl-devel readline-devel sqlite-devel tk-devel xz-devel zlib-devel wget git
```

## 2. 
```bash
git config --global user.name "remote"
git config --global user.email "1812673119@qq.com"
```


## 2. python 环境配置及有关问题修复
### python 环境安装 
> Pyhon 3.8.5 
```bash
# 安装python3.8.5 
bash init.sh
```
### python 环境问题修复
1. "_Ctype" 问题 已修复( `init.sh` 脚本已修复)
[参考链接](https://blog.csdn.net/qq_41794040/article/details/107205235)
```bash
# 操作流程
# 1.使用yum命令下载包
yum install libffi-devel -y
# 2.进入到Python的解压路径/usr/local/python3.8.3下，重新编译安装。
# ./configure prefix=/usr/local/python3
make && make install
```

2. 在当前文件夹下生成`venv`环境
```bash
# 生成 venv 环境
python3.8 -m venv venv

# pip 全局切换清华源
pip3.8 config set global.index-url https://pypi.tuna.tsinghua.edu.cn/simple
```

## Redis 缓存数据库安装及基本脚本
### Redis 安装
```bash
# 安装redis
yum install redis
# 启动redis
systemctl start redis
# 查看redis状态
systemctl status redis
# 设置开机启动
systemctl enable redis
```

```bash
# 启动
systemctl start redis
# 查看状态
systemctl status redis
# 停止
systemctl stop redis
# 重启
systemctl restart redis
```

```bash
redis-server -v
redis-server --version
```
- 输出

    Redis server v=3.2.12 sha=00000000:0 malloc=jemalloc-3.6.0 bits=64 
    build=7897e7d0e13773f

## 数据库安装及基本脚本
### SQLit3 安装
```bash
# 安装sqlite3
yum install sqlite-devel

# 检查是否安装成功
sqlite3 -version
```
