wget https://nodejs.org/dist/v18.16.0/node-v18.16.0-linux-x64.tar.xz
tar -xvf node-v18.16.0-linux-x64.tar.xz
mkdir -p /usr/local/nodejs
mv node-v18.16.0-linux-x64/* /usr/local/nodejs/
# 建立node软链接
ln -s /usr/local/nodejs/bin/node /usr/local/bin
# 建立npm 软链接
ln -s /usr/local/nodejs/bin/npm /usr/local/bin
# 设置国内淘宝镜像源
npm config set registry https://registry.npm.taobao.org
# 查看设置信息
npm config list
node -v
npm -v