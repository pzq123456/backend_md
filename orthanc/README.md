# init 

``` bash
docker run --rm --entrypoint=cat jodogne/orthanc-plugins /etc/orthanc/orthanc.json > /home/backend_md/orthanc/tmp/orthanc.json
```

``` bash 
docker run -p 4242:4242 -p 8042:8042 --rm -v /home/backend_md/orthanc/tmp/orthanc.json:/etc/orthanc/orthanc.json:ro jodogne/orthanc-plugins
```

