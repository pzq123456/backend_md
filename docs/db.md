# 数据库设计
> 本文档记录数据库设计思路，以及数据库表结构设计。
> - Pan 2023.5.4 23:08
> - 一切以项目实施为准，本文档仅供参考。
> - [深入理解数据库关系](https://luhuisicnu.gitbook.io/the-flask-mega-tutorial-zh/di-ba-zhang-fen-si)
## 1.表(字段)设计

> [参考地址](https://github.com/reactmed/neurdicom/blob/master/ndicom_server/apps/core/models.py)
> - 上述地址是对方的数据库设计，我们可以参考（照抄）一下


### 0.一些说明
1. 该平台面向医学影像研究者及有关人员，因此，数据库设计应该尽可能符合医学影像研究者的使用习惯。User （目标用户就是医师及相关研究人员）表用于实现 API 访问控制（使用 token），用户模型本身也便于管理和组织数据。
2. 一个医生可以有多个study(patient)，一个检查可以有多个序列，一个序列可以有多个实例。
3. patient 与 study 一对一，study 与 series 一对多，series 与 instance 一对多。
   ```mermaid
   graph LR
   A[医生User] --> B[study1]
    A --> C[study2]
    B --> D[series1]
    B --> E[series2]
    C --> F[series3]
    D --> G[instance1]
    D --> H[instance2]
    E --> I[instance3]
    F --> J[instance4]
    ```







### 1. 用户表

> [参考](https://luhuisicnu.gitbook.io/the-flask-mega-tutorial-zh/di-er-shi-san-zhang-ying-yong-cheng-xu-bian-cheng-jie-kou-api#api-ren-zheng)
> - 用户表用以实现 API 访问控制（使用 token）。
> - 用户模型本身也便于管理和组织数据。
```python
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    # 用于验证用户的token
    token = db.Column(db.String(32), index=True, unique=True)
    token_expiration = db.Column(db.DateTime)
```
- `id`：用户 ID，主键，自增
- `username`：用户名，唯一
- `email`：邮箱，唯一
- `password_hash`：密码哈希值
- `token`：用于验证用户的 token
- `token_expiration`：token 过期时间

```mermaid
classDiagram
class User{
    id: int
    username: str
    email: str
    password_hash: str
    token: str
    token_expiration: datetime
}
```
### 2. DICOM 模型
``` python
class DicomModel(models.Model):
    class Meta:
        abstract = True

    @classmethod
    def from_dataset(cls, model_instance=None, ds: Dataset = None):
        if model_instance is None:
            model_instance = cls()
        fields = cls._meta.get_fields()
        for field in fields:
            if isinstance(field, ManyToOneRel):
                continue
            if isinstance(field, ForeignKey) or isinstance(field, OneToOneField):
                continue
            elif isinstance(field, models.AutoField):
                continue
            elif isinstance(field, models.FileField) or isinstance(field, models.ImageField):
                continue
            elif isinstance(field, models.DateField):
                date_field_value = ds.get(FIELDS_TO_TAG[field.name], None)
                if date_field_value is None:
                    continue
                match = DICOM_DATE_REGEX.match(date_field_value)
                if match:
                    year, month, day = match.group(1), match.group(2), match.group(3)
                    setattr(model_instance, field.name, datetime.date(int(year), int(month), int(day)))
            elif isinstance(field, models.TimeField):
                pass  # To implement lately
            else:
                setattr(model_instance, field.name, ds.get(FIELDS_TO_TAG[field.name], None))
        return model_instance
```
- `from_dataset`：从数据集中获取数据，填充模型实例
- `cls`：模型类
- `model_instance`：模型实例
- `ds`：数据集

```mermaid
classDiagram
class DicomModel{
    from_dataset(cls, model_instance=None, ds: Dataset = None)
}
```



### 3. Patient 表
```py
    patient_name = models.CharField(verbose_name=_('Patient'), max_length=100, blank=True, null=True)
    patient_id = models.CharField(verbose_name=_('Patient ID'), max_length=100, blank=True, null=True)
    patient_sex = models.CharField(verbose_name=_('Patient Sex'), max_length=10, blank=True, null=True)
    patient_age = models.CharField(verbose_name=_('Patient Age'), max_length=30, blank=True, null=True)
    patient_birthdate = models.DateField(verbose_name=_('Patient Birthdate'), blank=True, null=True)
```
- `patient_name`：患者姓名
- `patient_id`：患者 ID
- `patient_sex`：患者性别
- `patient_age`：患者年龄
- `patient_birthdate`：患者出生日期

```mermaid
classDiagram
class Patient{
    patient_name: str
    patient_id: str 
    patient_sex: str
    patient_age: str
    patient_birthdate: datetime
}
```



### 4. Study 表
```py
    study_instance_uid = models.CharField(verbose_name=_('Study Instance UID'), max_length=80, unique=True)
    study_id = models.CharField(verbose_name=_('Study ID'), max_length=100, blank=True, null=True)
    study_date = models.DateField(verbose_name=_('Study Date'), blank=True, null=True)
    study_description = models.CharField(verbose_name=_('Study Description'), max_length=300, blank=True, null=True)
    referring_physician_name = models.CharField(verbose_name=_('Referring Physician'), max_length=100, blank=True,
                                                null=True)
    patient = models.ForeignKey('core.Patient', related_name='studies', on_delete=models.CASCADE)
```
- `study_instance_uid`：检查实例 ID
- `study_id`：检查 ID
- `study_date`：检查日期
- `study_description`：检查描述
- `referring_physician_name`：检查医生
- `patient`：患者

```mermaid
classDiagram
class Study{
    study_instance_uid: str
    study_id: str
    study_date: datetime
    study_description: str
    referring_physician_name: str
    patient: Patient
}
```

### 5. Series 表
```py
    series_instance_uid = models.CharField(verbose_name=_('Series Instance UID'), max_length=80, unique=True)
    protocol_name = models.CharField(verbose_name=_('Protocol Name'), max_length=150, blank=True, null=True)
    modality = models.CharField(verbose_name=_('Modality'), max_length=80)
    series_number = models.CharField(verbose_name=_('Series Number'), max_length=80, blank=True, null=True)
    patient_position = models.CharField(verbose_name=_('Patient Position'), max_length=30)
    body_part_examined = models.CharField(verbose_name=_('Body Part Examined'), max_length=50, blank=True, null=True)
    study = models.ForeignKey('core.Study', related_name='series', on_delete=models.CASCADE)
```
- `series_instance_uid`：序列实例 ID
- `protocol_name`：协议名称
- `modality`：设备类型
- `series_number`：序列号
- `patient_position`：患者体位
- `body_part_examined`：检查部位
- `study`：检查

```mermaid
classDiagram
class Series{
    series_instance_uid: str
    protocol_name: str
    modality: str
    series_number: str
    patient_position: str
    body_part_examined: str
    study: Study
}
```

### 6. Instance 表
```py
    sop_instance_uid = models.CharField(verbose_name=_('SOP Instance UID'), max_length=80, unique=True)
    instance_number = models.IntegerField(verbose_name=_('Instance Number'))
    rows = models.IntegerField(verbose_name=_('Rows'))
    columns = models.IntegerField(verbose_name=_('Columns'))
    color_space = models.CharField(verbose_name=_('Color Space'), max_length=30, blank=True, null=True)
    photometric_interpretation = models.CharField(verbose_name=_('Photometric Interpretation'), max_length=30,
                                                  blank=True, null=True)
    smallest_image_pixel_value = models.PositiveIntegerField(verbose_name=_('Smallest Image Pixel Value'), blank=True,
                                                             null=True)
    largest_image_pixel_value = models.PositiveIntegerField(verbose_name=_('Largest Image Pixel Value'), blank=True,
                                                            null=True)
    pixel_aspect_ratio = models.CharField(verbose_name=_('Pixel Aspect Ratio'), max_length=30, blank=True, null=True)
    pixel_spacing = models.CharField(verbose_name=_('Pixel Spacing'), max_length=30, blank=True, null=True)
    image = models.FileField(upload_to=image_file_path)
    series = models.ForeignKey('core.Series', related_name='instances', on_delete=models.CASCADE)
```
- `sop_instance_uid`：SOP 实例 ID
- `instance_number`：实例号
- `rows`：行数
- `columns`：列数
- `color_space`：颜色空间
- `photometric_interpretation`：光度解释
- `smallest_image_pixel_value`：最小像素值
- `largest_image_pixel_value`：最大像素值
- `pixel_aspect_ratio`：像素长宽比
- `pixel_spacing`：像素间距
- `image`：图像
- `series`：序列

```mermaid
classDiagram
class Instance{
    sop_instance_uid: str
    instance_number: int
    rows: int
    columns: int
    color_space: str
    photometric_interpretation: str
    smallest_image_pixel_value: int
    largest_image_pixel_value: int
    pixel_aspect_ratio: str
    pixel_spacing: str
    image: FileField
    series: Series
}
```

### 7. DICOM Node
```py
class DicomNode(models.Model):
    class Meta:
        db_table = 'dicom_nodes'

    name = models.CharField(verbose_name=_('Name'), max_length=255, blank=True, null=True)
    remote_url = models.CharField(verbose_name=_('Peer URL'), max_length=255)
    instances_url = models.CharField(verbose_name=_('WADO Instances URL'), default='/instances', max_length=255)
    instance_file_url = models.CharField(verbose_name=_('WADO Instance Image URL'), default='/instances/{id}/file',
                                         max_length=255)
```
- `name`：名称
- `remote_url`：远程 URL
- `instances_url`：WADO 实例 URL
- `instance_file_url`：WADO 实例图像 URL

```mermaid
classDiagram
class DicomNode{
    name: str
    remote_url: str
    instances_url: str
    instance_file_url: str
}
```

### 8. ProcessingResult
```py
class ProcessingResult(models.Model):
    class Meta:
        db_table = 'processing_results'

    expire_date = models.DateTimeField()
    image = models.FileField(upload_to=processed_image_path, verbose_name=_('Image'), null=True, blank=True)
    json = JSONField(verbose_name=_('JSON'))
    session_token = models.CharField(max_length=255)
```
- `expire_date`：过期时间
- `image`：图像
- `json`：JSON
- `session_token`：会话 token

```mermaid
classDiagram
class ProcessingResult{
    expire_date: datetime
    image: FileField
    json: JSONField
    session_token: str
}
```

## 2. 数据库关系

```mermaid
classDiagram
class User{
    id: int
    username: str
    email: str
    password_hash: str
    token: str
    token_expiration: datetime
}
class Patient{
    patient_name: str
    patient_id: str 
    patient_sex: str
    patient_age: str
    patient_birthdate: datetime
}
class ProcessingResult{
    expire_date: datetime
    image: FileField
    json: JSONField
    session_token: str
}
class DicomNode{
    name: str
    remote_url: str
    instances_url: str
    instance_file_url: str
}
class Instance{
    sop_instance_uid: str
    instance_number: int
    rows: int
    columns: int
    color_space: str
    photometric_interpretation: str
    smallest_image_pixel_value: int
    largest_image_pixel_value: int
    pixel_aspect_ratio: str
    pixel_spacing: str
    image: FileField
    series: Series
}
class Series{
    series_instance_uid: str
    protocol_name: str
    modality: str
    series_number: str
    patient_position: str
    body_part_examined: str
    study: Study
}
class Study{
    study_instance_uid: str
    study_id: str
    study_date: datetime
    study_description: str
    referring_physician_name: str
    patient: Patient
}
User "1" --> "N" ProcessingResult
User "1" --> "N" DicomNode
DicomNode "1" --> "N" Instance
Patient "1" --> "1" Study
Study "1" --> "N" Series
Series "1" --> "N" Instance
```

### API 列表
> reference :
> - [WADO-RS](https://www.dicomstandard.org/using/dicomweb/retrieve-wado-rs-and-wado-uri/)
> - [chat-sheet](https://www.dicomstandard.org/docs/librariesprovider2/dicomdocuments/dicom/wp-content/uploads/2018/04/dicomweb-cheatsheet.pdf?sfvrsn=4ef48ea0_0)
> - [STOW-RS](https://www.dicomstandard.org/using/dicomweb/store-stow-rs)

- Web 访问 DICOM 对象（WADO）使您能够通过引用检索特定的研究、系列和实例。WADO-RS 的规范可以在 [PS3.18 10.4](https://www.dicomstandard.org/using/d-icomweb/retrieve-wado-rs-and-wado-uri/) 中找到。WADO-RS 可以返回`二进制` DICOM 实例，以及`渲染`实例。
- Store (STOW-RS) Web 存储（STOW）使您能够将特定实例存储到服务器。规范可以在 [PS3.18 10.5](https://dicom.nema.org/medical/dicom/current/output/html/part18.html#sect_10.5) 中找到。
#### Methods
> WADO-RS

| Verb | Path | Description |
| ---- | ---- | ----------- |
| GET | {s}/studies/{study} | Retrieve entire study |
| GET | {s}/studies/{study}/rendered | Retrieve rendered study |
| GET | {s}/studies/{study}/series/{series} | Retrieve entire series |
| GET | {s}/studies/{study}/series/{series}/rendered | Retrieve rendered series |
| GET | {s}/studies/{study}/series/{series}/metadata | Retrieve series metadata |
| GET | {s}/studies/{study}/series/{series}/instances/{instance} | Retrieve instance |
| GET | {s}/studies/{study}/series/{series}/instances/{instance}/rendered | Retrieve rendered instance |
| GET | {s}/studies/{study}/series/{series}/instances/{instance}/metadata | Retrieve instance metadata |
| GET | {s}/studies/{study}/series/{series}/instances/{instance}/frames/{frames} | Retrieve frames in an instance |
| GET | {s}/{bulkdataURIReference} | Retrieve bulk data |
> STOW-RS

| Verb | Path | Description |
| ---- | ---- | ----------- |
| POST | {s}/studies | Store instances |
| POST | {s}/studies/{study} | Store instances |

> - 需要生成唯一标识符.要使用标准 UUID 生成生成 UID
> - 上传内容时，需要指定SOP类(该类的属性是DICOM类的子集)。
>   -有损JPEG SOP类使用ID“1.2.840.10008.1.2.4.50”。其他类型在PS 3.6的附录A中定义。

#### Rendered WADO-RS Query Parameters(用于渲染的 WADO-RS 查询参数)
| Key | Value | Description |
| --- | ----- | ----------- |
| annotation | “patient” / “technique” | Add burned-in demographics / procedure details |
| quality | {n} | Quality of image (lossy factor) |
| viewport | vw,vh / sx,sy,sw,sh | Width and height, or crop to specific region |
| window | centre,width,shape | Center of the range of gray scale in the image |
#### Accept Headers

| Category | Media Type | Support |
| -------- | ---------- | ------- |
| Single Frame Image | image/jpeg | default |
|  | image/gif | required |
|  | image/png | required |
|  | image/jp2 | optional |
| Multi-frame Image | image/gif | optional |
| Video | video/mpeg | optional |
|  | video/mp4 | optional |
|  | video/H265 | optional |
| Text | text/html | default |
|  | text/plain | required |
|  | text/xml | required |
|  | text/rtf | optional |
|  | application/pdf | optional |


## 3. 创新点 ModelZoo 的后端设计
### 0. 一些说明
1. ModelZoo 主要是为了管理不同的智能模型。
2. ModelZoo 是一片独立的区域，与其他区域无关。任何已登录的用户都可以使用 ModelZoo。
3. ModelZoo 由 Model 组成，Model 就是一个人工智能模型。

### 1. Model 表
- `model_name`：模型名称
- `model_id`：模型 ID
- `model_description`：模型描述
- `model_file_path`：模型文件路径
- `model_version`：模型版本
- `model_params_path`：模型参数路径

```mermaid
classDiagram
class Model{
    model_name: str
    model_id: str
    model_description: str
    model_file_path: str
    model_version: str
    model_params_path: str
}
```
