# A3-基于百度飞桨的 3D 医疗数据解析平台 SKDDX20 组
> 主项目链接 [MIP](https://github.com/pzq123456/MIP/tree/master)

## 后端部分
> - 后端负责人： 韦立楠 
> - 任务:
> - 使用 Flask 开发 RESTful Api 供前端调用,使用 Redis 缓存数据，使用 Celery 异步处理任务(也安装了node.js)


## 快速访问
- [数据库设计文档](docs/db.md)


## 项目文件说明
1. docs 文件夹：存放项目文档
2. env 文件夹：存放项目环境初始配置脚本
3. backend 文件夹：存放项目源码

> - gitlab 我的账号密码 提交时直接用这个账号密码就行
```https://github.com/pzq123456/MIP/blob/master/README.md#gitlab-%E5%85%AC%E5%85%B1%E8%B4%A6%E5%8F%B7```
> - gitlab 这个库是公共的 不方便放密码

## 项目设计
> 一些资源链接
> - [异  步](https://juejin.cn/post/6992116138398187533)
> - [flask ](https://www.bilibili.com/video/BV1Y7411d7Ys?p=1)
> - [入 门 ](https://tutorial.helloflask.com/)
> - [flask RESTful](https://juejin.cn/post/7100060184906563598)
> - [全面教程](https://luhuisicnu.gitbook.io/the-flask-mega-tutorial-zh/)

> 也可以参考我之前写的项目代码，已经克隆到这个服务器上了。详见 `RSPLATFORM` 文件夹

## 附录
### 1. 云服务器开发指南
1.1. 首先使用 VScode 连接到服务器，打开文件夹:
> home 文件夹下可以看到所有项目文件夹
> 选择对应的项目文件夹，打开即可
直接进入项目文件夹:

    /home/backend_md/

1.2. python 解释器选择
> 选择项目文件夹后，VScode 会自动提示你选择 python 解释器
若没有提示，可以手动选择。以下是解释器路径
    
    /home/backend_md/backend/venv/bin/python



## 测试自动分割算法
1. 找到 `/home/project/backend_md/MedicalSeg/data` 文件夹。需要分割的文件在 imgs 文件夹下，分割结果在 output 文件夹下。
> 注意点：
> - 所有要分割的医疗影像夹都放在 imgs 文件夹下，并且必须以 "_0000.nii.gz" 结尾，不然报错。
> - 分割结果保存在 output 文件夹下，文件名为原文件名 + ".nii.gz"，例如 "0000.nii.gz" 的分割结果为 "0000_seg.nii.gz"。

2. 目前必须要进入到 `/home/project/backend_md/MedicalSeg` 文件夹下运行 `/home/project/backend_md/MedicalSeg/data/test.sh` 这个脚本，才能运行分割算法。脚本里面是一些必须要传入的参数。

3. 现在，我们已经成功运行了分割算法，下面我们需要将分割算法封装为一个接口，供前端调用。

## 建议 TODO
> - 腾讯云服务器选带有gpu的，并且一定要安装 cuda 和 cudnn。
1. 首先先确保算法可以跑起来。略去安装python环境的步骤（之前已经教过了）。我们快进到飞桨安装的步骤。
```shell
# 安装飞桨
pip install paddlepaddle-gpu==2.4.2.post112 -f https://www.paddlepaddle.org.cn/whl/linux/mkl/avx/stable.html
```
2. 然后安装 MedicalSeg 依赖
```shell
cd /home/project/backend_md/MedicalSeg
pip install -r requirements.txt
```
3. 跑测试命令
```shell
sh /home/project/backend_md/MedicalSeg/data/test.sh
```
4. 观察分割结果 `/home/project/backend_md/MedicalSeg/data/output` 有没有生成分割结果。如果有，说明算法可以跑起来了。

## 封装建议
- 跑起来之后，试试看能不能直接在 python 里面调用算法。如果可以，就用flask封装成接口。
- 先不着急写异步，先写同步的接口。异步的接口后面我来修改。